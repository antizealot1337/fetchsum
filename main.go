package main

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"flag"
	"fmt"
	"hash"
	"io"
	"net/http"
	"os"
	"path"
	"strings"
	"time"

	"bitbucket.org/antizealot1337/fetchsum/checksum"
)

func main() {
	var (
		chksum       checksum.Sum
		outfile      string
		keep         bool
		size         int64
		readamt      int64
		total        int64
		showprogress bool
	)

	// Setup command line arguments
	flag.Var(&chksum, "checksum", "The checksum to check against the download")
	flag.StringVar(&outfile, "out", outfile, "The output file. (Optional)")
	flag.BoolVar(&keep, "keep", keep, "Keep the file in cases of failure.")
	flag.BoolVar(&showprogress, "quiet", true, "Controls the progress output")

	// Parse the command line flags
	flag.Parse()

	// Get the url
	if flag.NArg() < 1 {
		fmt.Fprintln(os.Stderr, "No url provided")
		os.Exit(-1)
	} //if

	// Get the URL
	url := flag.Arg(0)

	// The digest for the download
	var digest hash.Hash

	// Determine the hash
	switch chksum.Type() {
	case checksum.MD5:
		digest = md5.New()
	case checksum.SHA1:
		digest = sha1.New()
	case checksum.SHA256:
		digest = sha256.New()
	case checksum.SHA512:
		digest = sha512.New()
	} //switch

	// Make request
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	} //if
	defer resp.Body.Close()

	// Attempt to get the content size
	fmt.Sscanf(resp.Header.Get("Content-Length"), "%d", &size)

	if outfile == "" {
		outfile = path.Base(url)
	} //if

	// Open a file for output
	f, err := os.Create(outfile)
	if err != nil {
		panic(err)
	} //if

	// Setup the writer
	var out io.Writer
	if digest == nil {
		out = f
	} else {
		out = io.MultiWriter(f, digest)
	} //if

	if showprogress {
		// Setup the progress channel
		progress := make(chan int64, 0)
		done := make(chan struct{}, 0)

		// Start downloading
		go copyprogress(out, resp.Body, progress, done)

		// Get the time
		last := time.Now()

		// Setup a timer
		tick := time.Tick(1 * time.Second)

	updateloop:
		for {
			longest := 0
			select {
			case cur := <-tick:
				// Build the string
				outstr := fmt.Sprintf("%s %s", progressbar(total, size), calcspeed(cur.Sub(last), readamt))

				// Check if this is the longest string thus far
				if len(outstr) > longest {
					longest = len(outstr)
				} else {
					outstr = outstr + strings.Repeat(" ", longest-len(outstr))
				} //if

				// Write the string
				fmt.Printf("\r%s", outstr)
				last = cur
				readamt = 0
			case read := <-progress:
				readamt += read
				total += read
			case <-done:
				break updateloop
			} //select
		} //for

		fmt.Printf("\r%s", progressbar(total, size))
		fmt.Println("\nDone!")
	} else {

		// Download the file
		_, err = io.Copy(out, resp.Body)
		if err != nil {
			panic(err)
		} //if
	} //if

	// Close the output file
	err = f.Close()
	if err != nil {
		panic(err)
	} //if

	// Preform the check
	if digest != nil {
		if sum := fmt.Sprintf("%x", digest.Sum(nil)); sum == chksum.String() {
			fmt.Println("Done.")
		} else {
			fmt.Printf("Checksum failed provided(%v) != calc(%s)\n", chksum, sum)
			if !keep {
				os.Remove(outfile)
			} //if
		} //fi
	} //if
} //func

func copyprogress(out io.Writer, in io.Reader, progress chan<- int64, done chan<- struct{}) {
	const maxwrite = 8192
	var read int64
	var err error

	for err == nil {
		read, err = io.CopyN(out, in, maxwrite)
		progress <- read
	} //for

	done <- struct{}{}
} //func

func progressbar(amount, size int64) string {
	if size == 0 {
		return fmt.Sprintf("Downloaded %d bytes", amount)
	} else {
		var bar [10]rune
		percent := int(float64(amount) / float64(size) * 10)
		for i := range bar {
			if i < percent {
				bar[i] = '='
			} else {
				bar[i] = ' '
			} //if
		} //for
		return fmt.Sprintf("[%s] %d/%d bytes", string(bar[:]), amount, size)
	} //if
} //func

func calcspeed(d time.Duration, read int64) string {
	return fmt.Sprintf("%0.1f b/s", float64(read)/float64(d)*float64(time.Second))
} //func

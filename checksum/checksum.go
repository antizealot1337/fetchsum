package checksum

// SumType is a type of checksum.
type SumType uint8

// The sum types.
const (
	Unknown SumType = iota
	MD5
	SHA1
	SHA256
	SHA512
)

// Sum represents a string representation of the checksum from a hashing
// implementation.
type Sum string

// Type guesses at the SumType based on the length of the string.
func (s Sum) Type() SumType {
	switch len(s) {
	case 32:
		return MD5
	case 40:
		return SHA1
	case 64:
		return SHA256
	case 128:
		return SHA512
	} //switch
	return Unknown
} //func

// String return the Sum as a string.
func (s Sum) String() string {
	return string(s)
} //func

// Set the Sum value
func (s *Sum) Set(st string) error {
	return nil
} //func

package checksum

import (
	"flag"
	"testing"
)

var _ flag.Value = (*Sum)(nil)

func TestSumType(t *testing.T) {
	var (
		none   Sum
		md5    Sum = "d41d8cd98f00b204e9800998ecf8427e"
		sha1   Sum = "7d97e98f8af710c7e7fe703abc8f639e0ee507c4"
		sha256 Sum = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
		sha512 Sum = "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e"
	)

	if expected, actual := Unknown, none.Type(); actual != expected {
		t.Errorf("Expected type %v but was %v", expected, actual)
	} //if

	if expected, actual := MD5, md5.Type(); actual != expected {
		t.Errorf("Expected type %v but was %v", expected, actual)
	} //if

	if expected, actual := SHA1, sha1.Type(); actual != expected {
		t.Errorf("Expected type %v but was %v", expected, actual)
	} //if

	if expected, actual := SHA256, sha256.Type(); actual != expected {
		t.Errorf("Expected type %v but was %v", expected, actual)
	} //if

	if expected, actual := SHA512, sha512.Type(); actual != expected {
		t.Errorf("Expected type %v but was %v", expected, actual)
	} //if
} //func

# Fetchsum
A program to download a file and check if against a provided hash string. Only
Go's standard library and implementations are used to calculate the hashes of
downloaded files.

## Usage
The typical usage is `fetchsum -checkusm [SUM] [HTTP/S ADDRESS]`. This will
download the file (assuming it exists) and checks it against the sum (assuming
that SUM is a valid checksum and there were no download errors). The output file
is determined from the address/URL but may be overridden with the `-o FILENAME` flag. Failing a check will cause the downloaded file to deleted unless the
`-keep` flag is provided on the command line.

## License
Copyright 2017 under the terms of the MIT license. See license file for more
information.
